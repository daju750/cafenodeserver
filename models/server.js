const express = require('express')
const cors = require('cors')
const app = express()
const {dbConnection} = require('../database/config')

///const bodyParser = require('body-parser');

class Server {

    constructor(){
        this.app = express();
        this.port = app.listen(process.env.PORT);

        this.paths = {
            auth: '/api/auth',
            usuarios: '/api/usuarios',
            productos: '/api/productos',
            categorias: '/api/categorias'
        }
        //this.usuariosPath = '/api/usuarios';
        //this.authPath = '/api/auth';
        
        //Database
        this.conectarDB();
        //Middlewares
        
        this.middlewares();
        this.routes();
    }

    async conectarDB(){
        await dbConnection()
    }

    middlewares(){
        this.app.use(cors());
        //this.app.use(express.urlencoded({ extended: false }));
        this.app.use(express.json());
        this.app.use(express.static('public'));
    }

    routes(){
        this.app.use(this.paths.auth,require('../routes/auth'));
        this.app.use(this.paths.usuarios,require('../routes/user'));
        this.app.use(this.paths.categorias,require('../routes/categorias'));
        this.app.use(this.paths.productos,require('../routes/productos'));
    }

    listen(){
        this.app.listen(this.port);
    }


}

module.exports = Server