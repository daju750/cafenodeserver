const {Schema, model} = require('mongoose');

const ProductoSchema = Schema({

    nombre:{
        type: String,
        required:[true, 'Nombre es Obligatorio'],
        unique:true
    },
    estado:{
        type: Boolean,
        default:true,
        required:true
    },
    usuario:{
        type:Schema.Types.ObjectId,
        ref: 'Usuario',
        required:true 
    },
    precio:{
        type: Number,
        default:0,
    },
    Categoria:{
        type: Schema.Types.ObjectId,
        ref: 'Categoria',
        required:true 
    },
    descripcion:{type:String},
    disponible:{type:Boolean,default:true}
})

ProductoSchemaSchema.methods.toJSON = function(){
    const {__v,estado, ...categoria} = this.toObject();
    //categoria.uid = _id;
    return categoria
}

module.exports = model('Producto',ProductoSchema);