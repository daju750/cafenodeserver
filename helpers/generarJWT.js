const jwt = require('jsonwebtoken')

const generarJWT = (uid='') =>{

    return new Promise((resolver,reject)=>{
        const payload = {uid}

        jwt.sign(payload,process.env.SECRETKEY,{
            expiresIn: '24h'
        },(err,token)=> {
            if(err){
                console.log(err);
                reject(err);
            }else{
                resolver(token);
            }
        })

    })

}

module.exports = {
    generarJWT
}