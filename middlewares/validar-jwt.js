const {response,request} = require('express')
const jwt = require('jsonwebtoken')
const Usuario = require('../models/usuario');

const validarJWT = async(req=request,res=response,next)=>{

    const token = req.header('x-token');

    if(!token){return res.status(401).json({msg:'Token Invalido'})}

    try{
        const {uid} =  jwt.verify(token,process.env.SECRETKEY);
        const usuario = await Usuario.findById(uid);
        
        if(!usuario){return res.status(401).json({msg:'Usuario Inexistente'})}
        if(!usuario.estado){return res.status(401).json({msg:'Usuario Inactivo'})}
        req.usuario = usuario;
        next();
    }catch(error){
        console.log(error);
        res.status(401).json({msg:'Token no Valido'})
    }

}

module.exports = {
    validarJWT
}