const {response,request} = require('express')

const EsAdminRol = (req=request,res=response,next) => {

    if(!req.usuario){
        return res.status(500).json({msg:'Se quiero verificar el Rol sin antes verifcar en token'})
    }

    const {rol,nombre} = req.usuario;

    if(rol !== 'ADMIN_ROL'){
        return res.status(401).json({msg:'EL usuario '+nombre+' no es Admin'})
    }

    next();
}


const tieneRol =(...roles) =>{
    return (req=request,res=response,next) => {

        if(!req.usuario){
            return res.status(500).json({msg:'Se quiero verificar el Rol sin antes verifcar en token'})
        }

        if(!roles.includes(req.usuario.rol)){
            return res.status(500).json({msg:'No tiene Rol Valido'})
        }

        next();
    }
}

module.exports = {
    EsAdminRol,tieneRol
}