const {Router} = require('express');
const {check} = require('express-validator');
const {validarJWT, validarCampos,EsAdminRol} = require('../middlewares');
const router =  Router();
const {existeCategoriaPorId} = require('../helpers/db-validators')
const {CrearCategoria,ActulizarCategoria,ListarCategorias,CategoriaId,DeleteCategoria} = require('../controllers/categorias')


router.get('/',validarJWT,ListarCategorias)

router.get('/:id',[
    validarJWT,
    check('id','Id Invalido').isMongoId(),
    check('id').custom(existeCategoriaPorId),
    validarCampos,
    ],CategoriaId)

router.post('/',[
    validarJWT,
    check('nombre','El nombre es Obligatorio').not().isEmpty(),
    validarCampos
    ],CrearCategoria
)

router.put('/:id',[
    validarJWT,
    check('id','Id Invalido').isMongoId(),
    check('nombre','El nombre es obligatorio').not().isEmpty(),
    validarCampos
    ],ActulizarCategoria
)

router.delete('/:id',[
    validarJWT,
    EsAdminRol,
    check('id','Id Invalido').isMongoId(),
    check('nombre','El nombre es Obligatorio').not().isEmpty(),
    check('id').custom(existeCategoriaPorId),
    validarCampos
    ],DeleteCategoria)


module.exports = router;