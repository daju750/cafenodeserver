const {Router} = require('express');
const {check} = require('express-validator');
const {esRoleValido,existeUsuarioPorId} = require('../helpers/db-validators');
const {usuarioGet,usuarioPost,usuarioPut,usuarioDelete} = require('../controllers/users');

const {validarCampos,validarJWT,EsAdminRol,tieneRol} = require('../middlewares');

const router =  Router();

router.get('/',usuarioGet)

router.post('/',[
    check('nombre','El nombre es obligatorio').not().isEmpty(),
    check('correo','El email es obligatorio').isEmail(),
    check('password','La contraseña debe de tener mas de 6 caracteres').isLength({min:6}),
    check('rol','Rol Invalido').isIn(['ADMIN_ROL','USER_ROLE']),
    validarCampos
    ],usuarioPost)

router.put('/:id',[
    check('id','Id Invalido').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    check('rol').custom(esRoleValido), 
    validarCampos
    ],usuarioPut)

router.delete('/:id',
    validarJWT,
    EsAdminRol,
    //tieneRol('ADMIN_ROLE'),
    check('id','Id Invalido').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    validarCampos,
    usuarioDelete)

module.exports = router;