const {response,request} = require('express');
const Categoria = require('../models/categoria');
//const Usuario = require('../models/usuario');

const ListarCategorias = async(req=request,res=response) => {

    const {limite = 5 , desde = 0} = req.query;
    const query = {estado: true};

    const [total,categorias] = await Promise.all([
        Categoria.countDocuments(query),
        Categoria.find(query)
        .populate('usuario','nombre')
        .skip(Number(desde))
        .limit(Number(limite))
    ]);

    res.json({
        total,
        categorias
    });
}


const CategoriaId = async(req=request,res=response) =>{
    const {id} = req.query;
    const categoria = await Categoria.findOne(id)
        .populate('usuario','nombre');
    res.json(categoria);
}

const CrearCategoria = async(req, res = response) => {

    const nombre = req.body.nombre.toUpperCase();
    const categoriaDB = await Categoria.findOne({nombre});

    if(categoriaDB){
        return res.status(400).json({
            msg: 'La categoria '+categoriaDB+' ya existe'
        })
    }

    const data = {
        nombre,
        usuario: req.usuario._id
    }

    const categoria = new Categoria(data);
    await categoria.save();

    res.status(201).json(categoria); 
}


const ActulizarCategoria = async(req=request,res=response)=>{
    //const nombre = req.body.nombre.toUpperCase();
    const {id} = req.params;
    const {estado, usuario, ...data} = req.body;
    data.nombre = data.nombre.toUpperCase();
    data.usuario = req.usuario._id;

    const categoria = await Categoria.findByIdAndUpdate(id,data);
    console.log(categoria);
    res.status(201).json(categoria);
}

const DeleteCategoria = async(req=request,res=response)=>{
    const {id} = req.params;
    const categoria = await Categoria.findByIdAndUpdate(id,{estado:false})
    
    res.status(201).json(categoria);
}

module.exports = {
    CrearCategoria,
    ActulizarCategoria,
    ListarCategorias,
    CategoriaId,
    DeleteCategoria
}