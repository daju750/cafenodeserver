const {response} = require('express');
const bcryptjs = require('bcryptjs');

const Usuario = require('../models/usuario');
const {generarJWT} = require('../helpers/generarJWT');
const login = async(req = response, res = response) => {
    
    const {correo,password}=req.body;

    try{
        //Verificar si el correo Existe
        const usuario = await Usuario.findOne({correo});
        if(!usuario){
            return res.status(400).json({msg:'Usuario-Password invalidados correo'})
        }
        //Si el usuarioesta activo
        if(!usuario.estado){
            return res.status(400).json({msg:'Usuario-Password invalidados estdo:false'})
        }
        //Verificar Contraseña
        const validPassword = bcryptjs.compareSync(password,usuario.password);
        if(!validPassword){return res.status(400).json({msg:'Usuario-Password invalidados password:'})}
        //Generar JWT
        const token = await generarJWT(usuario.id);

        res.json({
            msg:"Informacion Recibida",
            usuario,
            token
        })

    }catch(error){
        console.log(error);
        return res.status(500).json({msg:"Algo salio Mal"})
    }

}

module.exports = {
    login
}